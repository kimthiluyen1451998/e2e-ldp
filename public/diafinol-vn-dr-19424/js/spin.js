document.addEventListener('DOMContentLoaded', function () {
    let resultWrapper = document.querySelector("#POPUP306"); //show popup sau khi quay
    let wheel = document.querySelector(".ladi-spin-lucky-screen");
    let spinWrapper = document.querySelector("#SECTION321"); // section chứa vòng quay
    let orderBlock = document.querySelector("#SECTION150"); // section chứa form
    let cursor = document.querySelector(".ladi-spin-lucky-start");

    function spin() {
        if (wheel.classList.contains("rotated")) {
            resultWrapper.style.display = "block";
            $('#backdrop-popup').attr('style', 'display: block !important; background-color : rgba(0, 0, 0, 0.5)');
        } else {
            wheel.classList.add("super-rotation");
            setTimeout(function () {
                resultWrapper.style.display = "block";
                $('#backdrop-popup').attr('style', 'display: block !important; background-color : rgba(0, 0, 0, 0.5)');
            }, 8000);
            setTimeout(function () {
                spinWrapper.style.display = 'none';
                orderBlock.style.display = 'block';
            }, 10000);
            wheel.classList.add("rotated");
        }
    }


    cursor.addEventListener('click', spin);

    resultWrapper.addEventListener('click', function (e) {
            e.preventDefault();
            resultWrapper.style.display = 'none';
            $('#backdrop-popup').attr('style', 'display: none !important;');
            document.querySelector('#SECTION150').scrollIntoView({behavior: 'smooth', block: 'start'}); // điền id form
        })

});
