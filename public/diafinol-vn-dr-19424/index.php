<?php
//v5

session_start();
$_SESSION["pixel"] = $_GET['pixel'] ?? '';
$_SESSION["utm_source"] = $_GET['utm_source'] ?? '';
$_SESSION["aff_click_id"] = $_GET['aff_click_id'] ?? '';
$_SESSION["sub1"] = $_GET['sub1'] ?? '';
$_SESSION["custom1"] = $_GET['custom1'] ?? '';
$_SESSION["sub_id2"] = $_GET['sub_id2'] ?? '';
$_SESSION["sub_id3"] = $_GET['sub_id3'] ?? '';
$_SESSION["sub_id4"] = $_GET['sub_id4'] ?? '';
$_SESSION["sub_id5"] = $_GET['sub_id5'] ?? '';
$_SESSION["aff_param1"] = $_GET['aff_param1'] ?? '';
$_SESSION["aff_param2"] = $_GET['aff_param2'] ?? '';
$_SESSION["aff_param3"] = $_GET['aff_param3'] ?? '';
$_SESSION["aff_param4"] = $_GET['aff_param4'] ?? '';
$_SESSION["aff_param5"] = $_GET['aff_param5'] ?? '';

?>
<!DOCTYPE html><html lang="vi"><head>
        <meta charset="UTF-8">
        <title>Bệnh tiểu đường không đến từ đồ ngọt!</title>
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta name="description" content="Bệnh tiểu đường không đến từ đồ ngọt!">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="images/ministry_of_health_vietnam_logosvg-20230131064817-c4yej_1.webp">
        <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin="">
        <link rel="preload" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&amp;display=swap" as="style" onload="this.onload = null; this.rel = 'stylesheet';">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="js/cert-spin.css" rel="stylesheet" type="text/css">
        <script>
            function fdate(d) {
                let now = new Date();
                now.setDate(now.getDate() + d);
                let p = new Date(now.getTime() + d * 86400000);
                let day = now.getDate();
                if (now.getDate() < 10) day = "0" + now.getDate();
                let monthA = "01,02,03,04,05,06,07,08,09,10,11,12".split(",");
                document.write(
                    day + "." + monthA[now.getMonth()] + "." + now.getFullYear()
                );
            }
        </script>
    </head>
    
    <body class="lazyload">
        <svg xmlns="http://www.w3.org/2000/svg" style="width: 0px; height: 0px; position: absolute; overflow: hidden; display: none;">
            <symbol id="shape_bFUVDKrgkj" viewBox="0 0 100 100">
                <path d="M49.999,40.019c-5.503,0-9.98,4.478-9.98,9.981s4.478,9.981,9.98,9.981c5.504,0,9.982-4.478,9.982-9.981  S55.503,40.019,49.999,40.019z"></path>
            </symbol>
            <symbol id="shape_PnRBGLqMAu" viewBox="0 0 22 22">
                <description>Created with Sketch.</description>
                <defs>
                    <path id="a" d="M15,15.5999985 L15,14.5 L11.4984375,17 L11.4984374,3 L10.5,3 L10.5,17 L7,14.5 L7,15.5999985 L11,18.5 L15,15.5999985 Z"></path>
                </defs>
                <g stroke="none" stroke-width="1" fill-rule="evenodd" sketch:type="MSPage">
                    <g>
                        <use fill-rule="evenodd" sketch:type="MSShapeGroup" xlink:href="#a"></use>
                        <use xlink:href="#a"></use>
                    </g>
                </g>
            </symbol>
        </svg>
        <div class="ladi-wraper">
            <div id="SECTION330" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="GROUP330" class="ladi-element">
                        <div class="ladi-group">
                            <div id="PARAGRAPH330" class="ladi-element">
                                <div class="ladi-paragraph ladi-transition">Bài viết quảng cáo&nbsp; &nbsp;&nbsp;</div>
                            </div>
                            <div id="PARAGRAPH331" class="ladi-element">
                                <div class="ladi-paragraph ladi-transition">|&nbsp; Tiết lộ&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SECTION8" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="BOX9" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="GROUP320" class="ladi-element">
                        <div class="ladi-group">
                            <div id="BOX10" class="ladi-element">
                                <div class="ladi-box ladi-transition"></div>
                            </div>
                            <div id="HEADLINE16" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">Đăng ký</h3>
                            </div>
                        </div>
                    </div>
                    <div id="HEADLINE30" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-7)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE31" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bệnh tiểu đường không đến từ đồ ngọt! Giám đốc trung tâm nghiên cứu quốc gia đã chỉ ra kẻ thù chính của bệnh tiểu đường và chia sẻ cách đơn giản để kiểm soát lượng đường trong máu.<br></h3>
                    </div>
                    <div id="HEADLINE32" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">05:50&nbsp;</h3>
                    </div>
                    <div id="HEADLINE33" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition"><span style="background-color: rgb(255, 241, 23);">Bệnh nhân tiểu đường tại Việt Nam đang bị lừa! Không có loại thuốc nào trong các hiệu thuốc có thể chữa khỏi bệnh. Làm thế nào để giảm lượng đường trong máu xuống 4,5 mmol/L trong 2 giờ và thoát khỏi bệnh tiểu đường mãi mãi? Hãy đọc bài viết dưới đây để có được câu trả lời.</span></h3>
                    </div>
                    <div data-action="true" id="HEADLINE12" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đăng nhập&nbsp;</h3>
                    </div>
                    <div id="LINE13" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div data-action="true" id="SHAPE14" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(255, 255, 255, 1)">
                                <g>
                                    <rect x="-192" width="185" height="99"></rect>
                                    <rect y="-36" width="100" height="30"></rect>
                                    <text transform="matrix(1 0 0 1 66 -19.5)" font-family="'Helvetica'" font-size="2.4">http://thenounproject.com</text>
                                    <text transform="matrix(1 0 0 1 7.166 -24.5)">
                                        <tspan x="0" y="0" font-family="'Helvetica-Bold'" font-size="6.1578">The Noun Project</tspan>
                                        <tspan x="1.12" y="4.8" font-family="'Helvetica-Bold'" font-size="4">Icon Template</tspan>
                                    </text>
                                    <text transform="matrix(1 0 0 1 -178.5 10.5)" font-family="'Helvetica-Bold'" font-size="6.1578">Reminders</text>
                                    <line stroke="#FFFFFF" stroke-miterlimit="10" x1="8" y1="-14.5" x2="18" y2="-14.5"></line>
                                    <line stroke="#FFFFFF" stroke-miterlimit="10" x1="-179" y1="16.5" x2="-162" y2="16.5"></line>
                                    <g>
                                        <g>
                                            <g>
                                                <rect x="-170.802" y="31.318" width="8.721" height="8.642"></rect>
                                                <path d="M-164.455,42.312h4.747v-4.703h-4.747V42.312z M-159.266,42.749h-5.63V37.17h5.63V42.749      L-159.266,42.749z M-166.221,44.062h8.279v-8.203h-8.279V44.062L-166.221,44.062z M-157.5,44.5h-9.163v-9.079h9.163V44.5      L-157.5,44.5z"></path>
                                                <polygon points="-166.149,44.133 -166.292,43.991 -158.013,35.787 -157.871,35.929     "></polygon>
                                            </g>
                                        </g>
                                    </g>
                                    <rect x="-179" y="58" width="35" height="32.5"></rect>
                                    <text transform="matrix(1 0 0 1 -179 60.1572)">
                                        <tspan x="0" y="0" font-family="'Helvetica-Bold'" font-size="3">Strokes</tspan>
                                        <tspan x="0" y="5" font-family="'Helvetica'" font-size="2.4">Try to keep strokes at 4px</tspan>
                                        <tspan x="0" y="10" font-family="'Helvetica'" font-size="2.4">Minimum stroke weight is 2px</tspan>
                                        <tspan x="0" y="14.5" font-family="'Helvetica'" font-size="2.4">For thicker strokes use even</tspan>
                                        <tspan x="0" y="17.5" font-family="'Helvetica'" font-size="2.4">numbers: 6px, 8px etc.</tspan>
                                        <tspan x="0" y="22" font-family="'Helvetica-Bold'" font-size="2.4">Remember to expand strokes</tspan>
                                        <tspan x="0" y="25" font-family="'Helvetica-Bold'" font-size="2.4">before saving as an SVG</tspan>
                                    </text>
                                    <rect x="-136.5" y="58" width="35" height="32.5"></rect>
                                    <text transform="matrix(1 0 0 1 -136.5 60.1572)">
                                        <tspan x="0" y="0" font-family="'Helvetica-Bold'" font-size="3">Size</tspan>
                                        <tspan x="0" y="5" font-family="'Helvetica'" font-size="2.4">Cannot be wider or taller than</tspan>
                                        <tspan x="0" y="8.5" font-family="'Helvetica'" font-size="2.4">100px (artboard size)</tspan>
                                        <tspan x="0" y="13.5" font-family="'Helvetica'" font-size="2.4">Scale your icon to fill as much of</tspan>
                                        <tspan x="0" y="16.5" font-family="'Helvetica'" font-size="2.4">the artboard as possible</tspan>
                                    </text>
                                    <rect x="-94" y="58" width="35" height="32.5"></rect>
                                    <text transform="matrix(1 0 0 1 -94 60.1572)">
                                        <tspan x="0" y="0" font-family="'Helvetica-Bold'" font-size="3">Ungroup</tspan>
                                        <tspan x="0" y="5" font-family="'Helvetica'" font-size="2.4">If your design has more than one</tspan>
                                        <tspan x="0" y="8" font-family="'Helvetica'" font-size="2.4">shape, make sure to ungroup</tspan>
                                    </text>
                                    <rect x="-50" y="58" width="35" height="32.5"></rect>
                                    <text transform="matrix(1 0 0 1 -50 60.1572)">
                                        <tspan x="0" y="0" font-family="'Helvetica-Bold'" font-size="3">Save as</tspan>
                                        <tspan x="0" y="5" font-family="'Helvetica'" font-size="2.4">Save as .SVG and make sure</tspan>
                                        <tspan x="0" y="8" font-family="'Helvetica'" font-size="2.4">“Use Artboards” is checked</tspan>
                                    </text>
                                    <text transform="matrix(1.0074 0 0 1 -125.542 30.5933)" font-family="'Helvetica'" font-size="2.5731">100px</text>
                                    <text transform="matrix(1.0074 0 0 1 -41 39)" font-family="'Helvetica-Bold'" font-size="5.1462">.SVG</text>
                                    <rect x="-126.514" y="34.815" width="10.261" height="10.185"></rect>
                                    <rect x="-126.477" y="31.766" width="0.522" height="2.337"></rect>
                                    <rect x="-116.812" y="31.766" width="0.523" height="2.337"></rect>
                                    <rect x="-127" y="32.337" width="11.233" height="0.572"></rect>
                                    <g>
                                        <rect x="-83.805" y="33.844" width="10.305" height="10.156"></rect>
                                        <rect x="-76.809" y="28.707" width="3.308" height="3.261"></rect>
                                    </g>
                                    <rect x="-178.5" y="22.5" stroke="#FFFFFF" stroke-miterlimit="10" width="30" height="30"></rect>
                                    <rect x="-136.5" y="22.5" stroke="#FFFFFF" stroke-miterlimit="10" width="30" height="30"></rect>
                                    <rect x="-93.5" y="22.5" stroke="#FFFFFF" stroke-miterlimit="10" width="30" height="30"></rect>
                                    <rect x="-49.5" y="22.5" stroke="#FFFFFF" stroke-miterlimit="10" width="30" height="30"></rect>
                                </g>
                                <g>
                                    <path d="M81.405,76.481L62.013,57.089c3.19-4.216,5.085-9.467,5.085-15.162c0-13.891-11.26-25.151-25.151-25.151   c-13.891,0-25.151,11.26-25.151,25.151c0,13.891,11.26,25.151,25.151,25.151c5.675,0,10.91-1.881,15.118-5.051l19.397,19.396   c1.64,1.64,4.074,1.861,5.439,0.497C83.267,80.556,83.044,78.12,81.405,76.481z M41.947,60.845   c-10.446,0-18.916-8.471-18.916-18.917c0-10.448,8.47-18.917,18.916-18.917s18.917,8.47,18.917,18.917   C60.863,52.374,52.393,60.845,41.947,60.845z"></path>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE24" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">MENU&nbsp;</h3>
                    </div>
                    <div id="GROUP313" class="ladi-element">
                        <div class="ladi-group">
                            <div id="HEADLINE18" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">GÓC NHÌN&nbsp;</h3>
                            </div>
                            <div id="HEADLINE19" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">KINH TẾ&nbsp;</h3>
                            </div>
                            <div id="HEADLINE20" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">QUỐC TẾ&nbsp;</h3>
                            </div>
                            <div id="HEADLINE22" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">VĂN HÓA&nbsp;</h3>
                            </div>
                            <div id="HEADLINE23" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">KHOA HỌC VÀ SỨC KHỎE&nbsp;</h3>
                            </div>
                            <div id="HEADLINE25" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">ĐỌC</h3>
                            </div>
                            <div id="HEADLINE27" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">TV&nbsp;</h3>
                            </div>
                        </div>
                    </div>
                    <div id="SHAPE28" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(255, 255, 255, 1)">
                                <g>
                                    <path d="M2.8,20.3h93.1c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H2.8c-1.3,0-2.4,1.1-2.4,2.4C0.4,19.2,1.5,20.3,2.8,20.3z"></path>
                                    <path d="M95.9,48H2.8c-1.3,0-2.4,1.1-2.4,2.4c0,1.3,1.1,2.4,2.4,2.4h93.1c1.3,0,2.4-1.1,2.4-2.4C98.4,49.1,97.3,48,95.9,48z"></path>
                                    <path d="M95.9,79.8H2.8c-1.3,0-2.4,1.1-2.4,2.4s1.1,2.4,2.4,2.4h93.1c1.3,0,2.4-1.1,2.4-2.4S97.3,79.8,95.9,79.8z"></path>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE29" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">ĐIỂM ĐẾN&nbsp;</h3>
                    </div>
                    <div id="HEADLINE17" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">VIỆT NAM&nbsp;</h3>
                    </div>
                    <div id="HEADLINE21" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">THỂ THAO&nbsp;</h3>
                    </div>
                    <div id="IMAGE326" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SECTION34" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE35" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Hôm nay chúng ta sẽ nói về cách tránh các biến chứng nguy hiểm của bệnh tiểu đường và kiểm soát lượng đường trong máu với chuyên gia Bác sĩ Trương Hữu Khánh<span style="font-weight: bold;">, Giám đốc Trung tâm Nghiên cứu Quốc gia, Giáo sư, Bác sĩ</span><br></h3>
                    </div>
                    <div id="HEADLINE36" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Ông Trương Hữu Khánh là giáo sư, bác sĩ và là chuyên gia điều trị bệnh đái tháo đường hàng đầu<br></h3>
                    </div>
                    <div id="IMAGE317" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="NOTIFY330" class="ladi-element">
                        <div class="ladi-notify ladi-hidden">
                            <div class="ladi-notify-image">
                                <img src="fonts/notify.svg">
                            </div>
                            <div class="ladi-notify-title">Nội dung cột [Title]</div>
                            <div class="ladi-notify-content">Nội dung cột [Content]</div>
                            <div class="ladi-notify-time">Nội dung cột [Time]</div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SECTION37" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE38" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Dù có lịch trình rất dày đặc và hiếm khi trả lời phỏng vấn, nhưng thật may mắn bác sĩ Trương Hữu Khánh đã đồng ý tham gia buổi phỏng vấn của chúng tôi.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE39" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Trương Hữu Khánh là bác sĩ chuyên khoa Nội tiết nổi tiếng không chỉ ở Việt Nam mà còn ở khắp các quốc gia Đông Nam Á. Thưa ông, ông có thể chia sẻ thêm về bệnh tiểu đường được không? Bệnh tiểu đường là gì?<br></h3>
                    </div>
                    <div id="HEADLINE40" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đái tháo đường là bệnh nội tiết có liên quan đến rối loạn hấp thu glucose và phát triển do thiếu hụt hormone insulin tuyệt đối hoặc tương đối, gây tăng đường huyết, tức là tăng đường huyết kéo dài.<br></h3>
                    </div>
                    <div id="HEADLINE41" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition" data-cy="data1">"Glucose ở nồng độ cao ảnh hưởng xấu tới các tế bào sống."<br></h3>
                    </div>
                    <div id="HEADLINE42" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition"><span style="background-color: rgb(255, 22, 22);">Đái tháo đường bắt đầu với lượng đường trong máu tăng đột biến và không bao giờ chấm dứt. Qua thời gian, các triệu chứng tích tụ lại như một quả cầu tuyết.</span><br></h3>
                    </div>
                    <div id="HEADLINE43" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đái tháo đường gây rối loạn tất cả các loại chuyển hóa: chất bột đường, chất béo, chất đạm, chất khoáng và nước muối nên người bệnh lúc nào cũng khát và muốn ăn; do đó, người bệnh đi tiểu thường xuyên, đói liên tục và khô miệng. Ngoài ra người bệnh có thể giảm hoặc tăng cân. Mệt mỏi, nhức đầu, nhìn mờ và rối loạn giấc ngủ xảy ra.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE44" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Sau đó là hàng loạt các biến chứng nguy hiểm có thể xảy ra như suy thận mãn tính, tổn thương mạch máu, mù lòa, đau thắt ngực, xơ vữa động mạch, liệt dương, nhồi máu cơ tim, sa sút trí tuệ, bệnh đa dây thần kinh, hôn mê... Danh sách các biến chứng này cứ ngày một kéo dài. Thuật ngữ "bệnh tiểu đường" xuất phát từ tiếng Hy Lạp cổ đại: διαβαίνω (diavaíno), được dịch là "qua đời ". Một mô tả hoàn toàn chính xác về quá trình bệnh tật.<br></h3>
                    </div>
                    <div id="HEADLINE45" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Có phải bệnh tiểu đường xuất phát từ việc ăn quá nhiều đồ ngọt?<br></h3>
                    </div>
                    <div id="HEADLINE46" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Khi nói đến bệnh tiểu đường loại 2, nó có nhiều khả năng là do tuổi tác hơn là việc ăn quá nhiều đồ ngọt. Nguyên nhân của bệnh lý là sự chậm lại của các quá trình trao đổi chất trong cơ thể và sự suy giảm của hóa sinh nói chung.<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION47" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE48" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">"Bệnh tiểu đường chỉ xảy ra ở những người thường xuyên ăn đồ ngọt là một nhận định hoàn toàn sai lầm."<br></h3>
                    </div>
                    <div id="HEADLINE49" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Nó không liên quan đến đồ ngọt. Trên thực tế, BỆNH NHÂN TIỂU ĐƯỜNG VẪN HOÀN TOÀN CÓ THỂ ĂN&nbsp; ĐỒ NGỌT NẾU HỌ BIẾT CÁCH KIỂM SOÁT ĐƯỢC LƯỢNG ĐƯỜNG HUYẾT CỦA HỌ.<br></h3>
                    </div>
                    <div id="HEADLINE50" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tránh xa đồ ngọt cả đời sẽ không giúp bạn tránh xa bệnh tiểu đường. Ngay cả những người ăn uống lành mạnh cả đời cũng phải đối mặt với bệnh lý này như bất kỳ ai khác.<br></h3>
                    </div>
                    <div id="IMAGE318" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SECTION51" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE52" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Thưa bác sĩ Trương Hữu Khanh, ông là một người phản đối gay gắt việc điều trị bệnh tiểu đường bằng metformin. Ông có thể cho biết lý do tại sao?<br></h3>
                    </div>
                    <div id="HEADLINE53" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đúng là như vậy, tôi hoàn toàn phản đối việc sử dụng metformin trong điều trị bệnh tiểu đường. Metformin là một chất hóa học cực mạnh. Nó có thể phá hủy thận và gan. Bạn biết đấy, bệnh nhân tiểu đường không sống lâu. Việc uống metformin góp phần đáng kể vào việc rút ngắn tuổi thọ của họ. Nó giúp cân bằng đường huyết một cách nhanh chóng nhưng có thể giết chết cơ thể trong thời gian dài. Nó giống như điều trị cúm bằng bức xạ vậy!&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE54" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Sau khi sử dụng metformin, lượng đường trở lại bình thường một cách nhanh chóng, nhưng chỉ là tạm thời (bởi chất này còn trong máu). <span style="font-weight: bold;">CHẤT GÂY HẠI NÀY TÍCH LŨY DẦN DẦN TRONG CƠ THỂ.&nbsp;</span><br></h3>
                    </div>
                    <div id="HEADLINE55" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bệnh tiểu đường "nổi tiếng" với các biến chứng và metformin không thể loại trừ chúng. Nếu một người hiện đang dùng metformin và không điều trị thêm nữa, <span style="font-weight: bold;">HỌ CŨNG SẼ CẦN DÙNG INSULIN.</span><br></h3>
                    </div>
                    <div id="HEADLINE56" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Điều quan trọng cần hiểu khi dùng metformin (hoặc bất kỳ loại thuốc nào khác để giảm lượng đường tạm thời):<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION57" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE58" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">"BẠN ĐANG KHÔNG LÀM NHỮNG GÌ THỰC SỰ CẦN PHẢI LÀM ĐỂ ĐIỀU TRỊ KHỎI BỆNH."<br></h3>
                    </div>
                    <div id="HEADLINE59" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Phương pháp điều trị thích hợp cho bệnh đái tháo đường là gì?<br></h3>
                    </div>
                    <div id="HEADLINE60" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đái tháo đường ngày nay có một kẻ thù tồi tệ hơn. Nó giúp người bệnh chữa khỏi bệnh lý. Không chỉ làm giảm lượng đường tạm thời, như metformin, mà còn giúp ổn định lượng đường trong máu bằng cách bình thường hóa các quá trình trao đổi chất.<br></h3>
                    </div>
                    <div id="HEADLINE61" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">KẺ THÙ LỚN NHẤT CỦA BỆNH TIỂU ĐƯỜNG <span style="font-weight: normal;">Mellitus là một chất được gọi là stigma-hydrocalciferol. Công thức hóa học của nó là C29H46O. Nó là một dạng vitamin D đặc biệt dễ tiêu hóa. Chất này đem lại hiệu quả rất tốt trong việc điều trị bệnh tiểu đường, nhất là đái tháo đường liên quan đến tuổi tác .</span><br></h3>
                    </div>
                    <div id="HEADLINE62" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Ông có thể chia sẻ thêm về stigma-hydrocalciferol? Làm cách nào mà chất này giúp điều trị bệnh tiểu đường?<br></h3>
                    </div>
                    <div id="HEADLINE63" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Stigma-hydrocalciferol là các chất thuộc nhóm hydroxyl. Các chất này có khả năng xúc tác cho phản ứng thứ bảy của quá trình đường phân. Nói một cách đơn giản, Stigma-hydrocalciferol cung cấp sự phân hủy glucose toàn diện hơn với một lượng insulin tối thiểu. NÓ KHÔNG LÀM GIẢM KHẢ NĂNG KHÁNG INSULIN NHƯ METFORMIN ĐÃ LÀM (ĐIỀU TRỊ BỆNH BỆNH TIỂU ĐƯỜNG), NHƯNG NÓ TĂNG CƯỜNG CHÍNH PHẢN ỨNG GLYCOLYSIS. ĐÓ LÀ SỰ KHÁC BIỆT CƠ BẢN.<br></h3>
                    </div>
                    <div id="HEADLINE64" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Chất này mới được phát hiện gần đây gần đây tại Trung tâm Nghiên cứu Quốc gia. Trong thời gian này, các chế phẩm dựa trên nó đã được điều chế và cho thấy hiệu quả đáng ngạc nhiên. <span style="font-weight: bold; color: rgb(48, 128, 232);"><a href="#SECTION321" data-cy="clickCTA" target="_self">Diafinol</a></span> là sản phẩm mới nhất và tốt nhất hiện nay. Hàm lượng stigma-hydrocalciferol của nó là cao nhất. Ngoài ra, công thức có chứa một dạng oxit của chất này, hoạt tính sinh học và tinh khiết nhất. Điều này đảm bảo rằng Diafinol không gây hại cho thận hoặc gan của người bệnh.<br></h3>
                    </div>
                    <div id="HEADLINE66" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Phương pháp điều trị này có loại bỏ nhu cầu dùng metformin hàng ngày không?<br></h3>
                    </div>
                    <div id="HEADLINE67" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tất nhiên. Ngoài ra, người bệnh cũng không cần phải tiêm insulin trong tương lai.<br></h3>
                    </div>
                    <div id="HEADLINE68" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Có nên uống Diafinol (stigma-hydrocalciferol) liên tục hay một liệu trình là đủ?<br></h3>
                    </div>
                    <div id="HEADLINE69" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Diafinol được sử dụng để điều trị bệnh đái tháo đường. Và đây là một lợi ích tuyệt vời của stigma-hydrocalciferol như một sản phẩm điều trị.<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION70" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE71" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">"MỘT TRONG NHỮNG ĐẶC TÍNH NỔI BẬT NHẤT CỦA STIMA-HYDROCALCIFEROL: NÓ LÀ DẠNG KHÁC CỦA VITAMIN D, CÓ KHẢ NĂNG TÍCH LŨY TRONG MÔ CƠ."<br></h3>
                    </div>
                    <div id="HEADLINE72" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Điều này có nghĩa là một đợt điều trị kéo dài 6 tuần có thể "dự trữ" chất này trong cơ thể và nó có thể tồn tại trong 3-4 năm.<br></h3>
                    </div>
                    <div id="HEADLINE73" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition"><span style="font-weight: bold;">Ngoài stigmahydrocalciferol, Diafinol còn chứa khoảng ba mươi thành phần bổ sung được điều chế để tăng tốc độ phục hồi của cơ thể bệnh nhân tiểu đường trong chế độ ăn kiêng đường bình thường.</span> Đây là những vitamin thiết yếu, vi chất dinh dưỡng. Theo quy định, tất cả các vitamin này được kê đơn riêng trong điều trị bệnh tiểu đường, nhưng ở đây chúng đã được đưa vào sản phẩm theo tỷ lệ thích hợp.<br></h3>
                    </div>
                    <div id="HEADLINE106" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Và nhiều loại vitamin khác.<br></h3>
                    </div>
                    <div id="HEADLINE107" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Xin bác sĩ cho biết mức độ an toàn của phương pháp điều trị này?<br></h3>
                    </div>
                    <div id="HEADLINE108" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tất cả các loại thuốc trị đái tháo đường đều có nhiều chống chỉ định. Tuy nhiên Diafinol hoàn toàn không có. Đây là một ưu điểm lớn của phương pháp điều trị này. Như tôi đã nói trước đây, stigma-hydrocalciferol là một loại vitamin D đặc biệt. Và bản thân vitamin D cực kỳ có lợi cho bệnh nhân tiểu đường. Stigma-hydrocalciferol thậm chí còn hữu ích hơn. Đặc biệt sau khi điều chế.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE109" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Như tôi đã nói, Diafinol được tạo ra bởi viện nội tiết chính của nước ta chứ không phải bởi một công ty dược phẩm thương mại nào đó. Các nhà khoa học tầm cỡ thế giới đã tham gia vào việc tạo ra nó. Đó là lý do tại sao tôi khuyên mọi người muốn ngăn chặn sự phát triển của bệnh lý nên điều trị bằng stigmahydrocalciferol.<br></h3>
                    </div>
                    <div id="HEADLINE110" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Liệu bệnh tiểu đường có biến mất hoàn toàn sau khi điều trị?<br></h3>
                    </div>
                    <div id="HEADLINE111" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tất nhiên nó sẽ hoàn toàn biến mất. Bệnh tiểu đường sẽ biến mất trong 10 năm hoặc hơn. Hai hoặc ba năm lượng đường huyết không tăng, sức khỏe ổn định. Tôi sẽ mô tả chi tiết hơn một chút về những thay đổi tích cực về sức khỏe xảy ra sau khi dùng Diafinol.<br></h3>
                    </div>
                    <div id="LINE86" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="HEADLINE87" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Hoài sơn<br></h3>
                    </div>
                    <div id="HEADLINE90" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Lá neem<br></h3>
                    </div>
                    <div id="HEADLINE91" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đan sâm</h3>
                    </div>
                    <div id="HEADLINE92" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Linh chi<br></h3>
                    </div>
                    <div id="HEADLINE93" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Cỏ cà ri<br></h3>
                    </div>
                    <div id="HEADLINE94" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Kế sữa<br></h3>
                    </div>
                    <div id="HEADLINE95" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">ALA<br></h3>
                    </div>
                    <div id="HEADLINE96" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Dây thìa canh<br></h3>
                    </div>
                    <div id="HEADLINE97" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Thúc đẩy giải phóng GLP-1 và cải thiện chức năng của các tế bào β duy trì mức insulin<br></h3>
                    </div>
                    <div id="HEADLINE99" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Giảm hấp thụ glucose sau ăn nhờ ức chế enzym α-glucosidase và&nbsp; α-amylase<br></h3>
                    </div>
                    <div id="LINE75" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE76" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE77" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE78" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE79" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE80" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE81" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE82" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE83" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE84" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="LINE85" class="ladi-element">
                        <div class="ladi-line">
                            <div class="ladi-line-container"></div>
                        </div>
                    </div>
                    <div id="HEADLINE100" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bảo vệ huyết áp bằng cách giảm áp lực động mạch phổi</h3>
                    </div>
                    <div id="HEADLINE101" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Polysaccharide giảm lượng đường trong máu, loại bỏ khả năng nhiễm bệnh và hạn chế biến chứng<br></h3>
                    </div>
                    <div id="HEADLINE102" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Hỗ trợ giảm cholesterol trong máu và lipid máu, có tác dụng chống oxy hoá và ung thư<br></h3>
                    </div>
                    <div id="HEADLINE103" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Kiểm soát đái tháo đường loại 2, điều trị xơ gan và bảo vệ gan<br></h3>
                    </div>
                    <div id="HEADLINE104" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Giảm các triệu chứng tổn thương thần kinh ở bệnh tiểu đường<br></h3>
                    </div>
                    <div id="HEADLINE105" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Làm tăng tiết insulin tuyến tuỵ, tăng cường hoạt lực của insulin. Ức chế hấp thụ glucose ở ruột, làm giảm đường huyết sau ăn.&nbsp;<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION112" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE113" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Điều trị Diafinol giống như lần sinh thứ hai. Kết quả điều trị với sản phẩm này rất ấn tượng.<br></h3>
                    </div>
                    <div id="HEADLINE114" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Kết quả về hiệu quả của Diafinol đã thu được trong các thử nghiệm lâm sàng. Khoảng các kết quả tương tự cho thấy việc sử dụng Diafinol của bệnh nhân tiểu đường trong phòng khám.<br></h3>
                    </div>
                    <div id="HEADLINE115" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Kết quả đạt được sau khi sử dụng Diafinol:<br></h3>
                    </div>
                    <div id="HEADLINE116" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Giảm lượng đường trong máu về mức bình thường (không tăng đột biến) - 99% bệnh nhân.
<br>Cải thiện mạch máu và các cơ quan nội tạng (loại bỏ tác dụng của đường) - 99% bệnh nhân.
<br>Phục hồi tuyến tụy - 96% bệnh nhân.
<br>Loại bỏ tất cả các triệu chứng của bệnh đái tháo đường: khát nước, mệt mỏi nhanh chóng, đi tiểu thường xuyên, phù nề, tâm trạng xấu, v.v. - 96% bệnh nhân.
<br>Cải thiện thị lực và chức năng tình dục: 92% bệnh nhân.
<br>Cải thiện tình trạng của da (biến mất vết loét và ngứa) - 98% bệnh nhân.
<br>Cải thiện tình trạng thận (điều trị viêm bể thận) - 93% bệnh nhân.<br></h3>
                    </div>
                    <div id="SHAPE117" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE118" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE119" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE120" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE121" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE122" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE123" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE124" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Như bạn có thể thấy, <span style="font-weight: bold; color: rgb(56, 182, 255);"><a href="#SECTION321" target="_self" style="">Diafinol</a></span> giúp ích cho hầu hết bệnh nhân tiểu đường. Những ai khao khát thoát khỏi bệnh tiểu đường cũng đều thành công thoát khỏi nó. Diafinol hoạt động tốt cho cả bệnh tiểu đường nhẹ và trung bình. Ngay cả trong trường hợp bệnh tiểu đường nặng, nó cũng có kết quả đáng kể.<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION125" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE126" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tại sao các bác sĩ và dược sĩ nội tiết chưa cung cấp Diafinol cho bệnh nhân?<br></h3>
                    </div>
                    <div id="HEADLINE127" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Theo bạn thì tại sao.&nbsp;</h3>
                    </div>
                    <div id="HEADLINE128" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Có lẽ sản phẩm được sản xuất với số lượng hạn chế và không có đủ cho bệnh nhân tiểu đường cả nước?<br></h3>
                    </div>
                    <div id="HEADLINE129" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bạn nói đúng. Sản phẩm tốt đến nỗi mọi người mua từng đợt sản xuất, xếp hàng mua trước 6 tháng. Vì lý do này, Diafinol không tiếp cận được thị trường các hiệu thuốc toàn quốc.<br></h3>
                    </div>
                    <div id="HEADLINE130" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Nhưng tôi cũng có tin tốt cho độc giả của quý báo. Chúng tôi có một lô Diafinol muốn cung cấp cho độc giả của các bạn. Tuy nhiên số lượng không có nhiều, chỉ 1.270 gói. Diafinol chỉ có sẵn cho những ai đăng ký nhanh nhất. Chúng tôi cung cấp sản phẩm này với giá gốc, với chiết khấu bổ sung từ Quỹ Từ thiện Y tế (quỹ sẽ hỗ trợ phần giá chênh lệch). Số tiền giảm giá mà một người nhận được phụ thuộc vào vận may của họ. Với mức giảm giá tối đa 50%, bạn sẽ được mua <span style="font-weight: bold; color: rgb(48, 128, 232);"><a href="#SECTION321" target="_self">Diafinol</a></span> với giá rẻ nhất.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE131" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bạn có thể cho tôi biết thêm về những gì cần phải làm để mua được Diafinol với những ưu đãi hấp dẫn như vậy không?<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION132" class="ladi-section ldp-js-date2">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE133" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Lưu ý:<br></h3>
                    </div>
                    <div id="HEADLINE134" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition">Tham gia quay số may mắn để nhận được chiết khấu
<br>Bạn chỉ có thể đặt Diafinol đến hết <b class="today"></b>.<br>Nhận Diafinol sau 2-3 ngày và thanh toán tiền mặt khi nhận hàng (giao hàng không cần trả trước, nhận hàng mới thanh toán)<br></h3>
                    </div>
                    <div id="SHAPE135" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE136" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="SHAPE137" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#E868B6">
                                <use xlink:href="#shape_bFUVDKrgkj"></use>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE138" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đó là tất cả những điều bạn cần lưu ý.<br></h3>
                    </div>
                    <div id="HEADLINE139" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Cảm ơn ông Trương Hữu Khánh rất nhiều vì một cuộc phỏng vấn chi tiết như vậy. Có điều gì ông muốn nói với độc giả của chúng tôi trước khi buổi phỏng vấn kết thúc không?<br></h3>
                    </div>
                    <div id="HEADLINE140" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tôi xin chúc tất cả mọi người có sức khỏe tốt. Và để nhớ rằng BỆNH TIỂU ĐƯỜNG SẼ NẶNG THÊM THEO TỪNG PHÚT! Và bệnh tiểu đường không bao giờ tự biến mất. Diafinol là phương thuốc duy nhất có thể giúp bạn.<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION321" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="BOX322" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div id="SPINLUCKY323" class="ladi-element">
                        <div class="ladi-spin-lucky">
                            <div class="ladi-spin-lucky-screen">
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-11.25deg) translateY(-50%); -webkit-transform: rotate(-11.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-33.75deg) translateY(-50%); -webkit-transform: rotate(-33.75deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-56.25deg) translateY(-50%); -webkit-transform: rotate(-56.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-78.75deg) translateY(-50%); -webkit-transform: rotate(-78.75deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-101.25deg) translateY(-50%); -webkit-transform: rotate(-101.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-123.75deg) translateY(-50%); -webkit-transform: rotate(-123.75deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-146.25deg) translateY(-50%); -webkit-transform: rotate(-146.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-168.75deg) translateY(-50%); -webkit-transform: rotate(-168.75deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-191.25deg) translateY(-50%); -webkit-transform: rotate(-191.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-213.75deg) translateY(-50%); -webkit-transform: rotate(-213.75deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-236.25deg) translateY(-50%); -webkit-transform: rotate(-236.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-258.75deg) translateY(-50%); -webkit-transform: rotate(-258.75deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-281.25deg) translateY(-50%); -webkit-transform: rotate(-281.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-303.75deg) translateY(-50%); -webkit-transform: rotate(-303.75deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-326.25deg) translateY(-50%); -webkit-transform: rotate(-326.25deg) translateY(-50%);"></div>
                                <div class="ladi-spin-lucky-label" style="transform: rotate(-348.75deg) translateY(-50%); -webkit-transform: rotate(-348.75deg) translateY(-50%);"></div>
                            </div>
                            <div class="ladi-spin-lucky-start"></div>
                        </div>
                    </div>
                    <div id="PARAGRAPH325" class="ladi-element">
                        <div class="ladi-paragraph ladi-transition"><span style="font-weight: 700;">Quan trọng! Bạn có thể nhận được giảm giá thêm khi mua Diafinol. Nhấp vào nút quay dưới bánh xe nằm bên dưới để thử vận ​​​​may của bạn. Tùy thuộc vào mức chiết khấu mà bạn nhận được, cùng với đó bạn có thể đặt hàng Diafinol. Mã giảm giá được cung cấp bởi Tổ chức "Đẩy lùi bệnh tiểu đường". Mức chiết khấu tối đa là 50%.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SECTION150" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="IMAGE152" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE154" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Giá cũ: 1.180.000 VNĐ</h3>
                    </div>
                    <div id="GROUP155" class="ladi-element">
                        <div class="ladi-group">
                            <div id="HEADLINE156" class="ladi-element ldp-js-date2">
                                <h3 class="ladi-headline ladi-transition"><span style="font-weight: bold;">Chứng nhận hàng chính hãng</span></h3>
                            </div>
                            <div id="SHAPE157" class="ladi-element">
                                <div class="ladi-shape ladi-transition">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(17, 123, 42, 1)">
                                        <path d="M28.245,4.86C20.391,9.676,14.691,15.751,12.131,18.8L5.86,13.887L3.09,16.12l10.836,11.02  c1.865-4.777,7.771-14.113,14.983-20.746L28.245,4.86z"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="IMAGE160" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE161" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Điền vào biểu mẫu<br></h3>
                    </div>
                    <div id="HEADLINE162" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition">Nhận ngay <span style="background-color: rgb(253, 245, 245);">Diafinol</span> với giá <span style="color: rgb(218, 26, 15);" class="hh">590.000 VNĐ</span><br></h3>
                    </div>
                    <div id="GROUP163" class="ladi-element">
                        <div class="ladi-group">
                            <div id="HEADLINE164" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">GIÁ&nbsp; ƯU ĐÃI 50% CHỈ CÒN</h3>
                            </div>
                            <div id="HEADLINE165" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">590.000 VNĐ</h3>
                            </div>
                        </div>
                    </div>
                    <div id="FORM39" data-config-id="5f054610d4234516b99b02b6" class="ladi-element">
                        <form autocomplete="off" method="post" class="ladi-form" action="send.php">
                            <div id="BUTTON167" class="ladi-element">
                                <button type="submit" class="ladi-button ladi-transition" data-cy="submit">
                                    <div class="ladi-button-background"></div>
                                    <div id="BUTTON_TEXT40" class="ladi-element ladi-button-headline">
                                        <p class="ladi-headline ladi-transition"><span class="ladipage-animated-headline clip"><span class="ladipage-animated-words-wrapper" data-word="[&quot;HOÀN TẤT ĐƠN ĐĂNG KÝ&quot;]">HOÀN TẤT ĐƠN ĐĂNG KÝ</span></span>
                                        </p>
                                    </div>
                                </button>
                            </div>
                            <div id="FORM_ITEM169" class="ladi-element">
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class="ladi-form-item">
                                        <input data-cy="name" name="name" required="" class="ladi-form-control" type="text" placeholder="Họ và Tên" value="">
                                    </div>
                                </div>
                            </div>
                            <div id="FORM_ITEM170" class="ladi-element">
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class="ladi-form-item">
                                        <input data-cy="phone" name="phone" required="" class="ladi-form-control" type="tel" placeholder="Số điện thoại" value="">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="aff_click_id" value="<?php echo $_SESSION["aff_click_id"]; ?>" />
                            <input type="hidden" name="sub1" value="<?php echo $_SESSION["sub1"]; ?>">
                            <input type="hidden" name="pixel" value="<?php echo $_SESSION["pixel"]; ?>">
                            <input type="hidden" name="custom1" value="<?php echo $_SESSION["custom1"]; ?>">
                            <input type="hidden" name="sub_id2" value="<?php echo $_SESSION["sub_id2"]; ?>">
                            <input type="hidden" name="sub_id3" value="<?php echo $_SESSION["sub_id3"]; ?>">
                            <input type="hidden" name="sub_id4" value="<?php echo $_SESSION["sub_id4"]; ?>">
                            <input type="hidden" name="sub_id5" value="<?php echo $_SESSION["sub_id5"]; ?>">
                            <input type="hidden" name="aff_param1" value="<?php echo $_SESSION["aff_param1"]; ?>">
                            <input type="hidden" name="aff_param2" value="<?php echo $_SESSION["aff_param2"]; ?>">
                            <input type="hidden" name="aff_param3" value="<?php echo $_SESSION["aff_param3"]; ?>">
                            <input type="hidden" name="aff_param4" value="<?php echo $_SESSION["aff_param4"]; ?>">
                            <input type="hidden" name="aff_param5" value="<?php echo $_SESSION["aff_param5"]; ?>">
                        </form>
                    </div>
                    <div id="HEADLINE171" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">*Số lượng sản phẩm phụ thuộc vào tình trạng và liệu trình điều trị của mỗi cá nhân<br></h3>
                    </div>
                    <div id="GROUP172" class="ladi-element">
                        <div class="ladi-group">
                            <div id="HEADLINE173" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">Nhập tên của bạn</h3>
                            </div>
                            <div id="SHAPE174" class="ladi-element">
                                <div class="ladi-shape ladi-transition">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" viewBox="0 0 22 22" version="1.1" x="0px" y="0px" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(48, 128, 232, 1)">
                                        <use xlink:href="#shape_PnRBGLqMAu"></use>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE175" class="ladi-element">
                                <div class="ladi-shape ladi-transition">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" viewBox="0 0 22 22" version="1.1" x="0px" y="0px" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(48, 128, 232, 1)">
                                        <use xlink:href="#shape_PnRBGLqMAu"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GROUP176" class="ladi-element">
                        <div class="ladi-group">
                            <div id="HEADLINE177" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">Nhập số điện thoại liên hệ</h3>
                            </div>
                            <div id="SHAPE178" class="ladi-element">
                                <div class="ladi-shape ladi-transition">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" viewBox="0 0 22 22" version="1.1" x="0px" y="0px" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(48, 128, 232, 1)">
                                        <use xlink:href="#shape_PnRBGLqMAu"></use>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE179" class="ladi-element">
                                <div class="ladi-shape ladi-transition">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" viewBox="0 0 22 22" version="1.1" x="0px" y="0px" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(48, 128, 232, 1)">
                                        <use xlink:href="#shape_PnRBGLqMAu"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GROUP180" class="ladi-element">
                        <div class="ladi-group">
                            <div id="HEADLINE181" class="ladi-element">
                                <h3 class="ladi-headline ladi-transition">Nhấn vào đây để đặt hàng</h3>
                            </div>
                            <div id="SHAPE182" class="ladi-element">
                                <div class="ladi-shape ladi-transition">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" viewBox="0 0 22 22" version="1.1" x="0px" y="0px" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(48, 128, 232, 1)">
                                        <use xlink:href="#shape_PnRBGLqMAu"></use>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE183" class="ladi-element">
                                <div class="ladi-shape ladi-transition">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" viewBox="0 0 22 22" version="1.1" x="0px" y="0px" preserveAspectRatio="none" width="100%" height="100%" class="" fill="rgba(48, 128, 232, 1)">
                                        <use xlink:href="#shape_PnRBGLqMAu"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HEADLINE184" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đơn hàng sẽ được chuyển MIỄN PHÍ ĐẾN TẬN NHÀ theo đúng yêu cầu của bạn.<br></h3>
                    </div>
                    <div id="IMAGE330" class="ladi-element">
                        <div class="ladi-image">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="IMAGE158" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SECTION185" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="SHAPE188" class="ladi-element">
                        <div class="ladi-shape ladi-transition">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" viewBox="0 0 101 81" version="1.1" x="0px" y="0px" preserveAspectRatio="none" width="100%" height="100%" class="" fill="#000">
                                <description>Created with Sketch (http://www.bohemiancoding.com/sketch)</description>
                                <g stroke="none" stroke-width="1" fill-rule="evenodd" sketch:type="MSPage">
                                    <path d="M3.97146349,0 C2.31494331,0 0.97206841,1.33787142 0.972068453,3.00222833 L0.972070141,67.9220881 C0.972070184,69.580173 2.31366578,70.9243164 3.96882602,70.9243164 L16.7075196,70.9243164 L29.337092,80.4900189 C30.215085,81.1550152 31.624361,81.1452197 32.4968325,80.45867 L44.6131474,70.9243164 L97.7410476,70.9243164 C99.3966395,70.9243164 100.738762,69.586445 100.738762,67.9220881 L100.73876,3.00222833 C100.73876,1.34414341 99.4038692,0 97.7393649,0 L3.97146349,0 Z M8.00596461,5 C6.89810095,5 6,5.88718193 6,6.9979447 L6,64.095205 C6,65.1986394 6.89550276,66.0931497 7.99736893,66.0931497 L18.1783447,66.0931497 L29.2663926,75.3848028 C30.114284,76.0953258 31.4969572,76.1100588 32.356397,75.4163238 L43.9064941,66.0931497 L94.005667,66.0931497 C95.1071067,66.0931497 96,65.2059677 96,64.095205 L96,6.9979447 C96,5.89451031 95.1092384,5 93.9940354,5 L8.00596461,5 Z M22.3773963,18.1672522 C21.8259374,18.1672522 21.3788917,18.6193432 21.3788917,19.1752277 L21.3788917,21.7010897 C21.3788917,22.2577792 21.8195246,22.7090653 22.3773963,22.7090653 L79.3334321,22.7090653 C79.884891,22.7090653 80.3319368,22.2569743 80.3319368,21.7010897 L80.3319368,19.1752277 C80.3319368,18.6185382 79.8913038,18.1672522 79.3334321,18.1672522 L22.3773963,18.1672522 Z M22.3773963,31.7926914 C21.8259374,31.7926914 21.3788917,32.2447823 21.3788917,32.8006669 L21.3788917,35.3265289 C21.3788917,35.8832184 21.8195246,36.3345044 22.3773963,36.3345044 L79.3334321,36.3345044 C79.884891,36.3345044 80.3319368,35.8824134 80.3319368,35.3265289 L80.3319368,32.8006669 C80.3319368,32.2439774 79.8913038,31.7926914 79.3334321,31.7926914 L22.3773963,31.7926914 Z M22.3810361,45.4181305 C21.827567,45.4181305 21.3788917,45.8702215 21.3788917,46.426106 L21.3788917,48.951968 C21.3788917,49.5086575 21.8338577,49.9599436 22.3810361,49.9599436 L63.4857502,49.9599436 C64.0392193,49.9599436 64.4878947,49.5078526 64.4878947,48.951968 L64.4878947,46.426106 C64.4878947,45.8694165 64.0329287,45.4181305 63.4857502,45.4181305 L22.3810361,45.4181305 Z M22.3810361,45.4181305" sketch:type="MSShapeGroup"></path>
                                    </g>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE189" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">BÌNH LUẬN<br></h3>
                    </div>
                    <div id="HEADLINE190" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tôi là một trong những người đã dùng thử Diafinol. Tôi đã điều trị với loại thuốc này vào năm ngoái. Một bác sĩ nội tiết khuyên tôi sử dụng khi nó lần đầu tiên xuất hiện. Bây giờ tôi cũng không bị tiểu đường nữa. Và tôi hoàn toàn khỏe mạnh! Diafinol đã chữa khỏi bệnh tiểu đường của tôi. Tôi đã giới thiệu thuốc này cho mọi người xung quanh. Nó thực sự rất tốt.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE191" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Metformin đang được các đại gia dược phẩm thúc đẩy bán vì chi phí sản xuất thấp!!! Họ chẳng thèm quan tâm đến sức khỏe của mọi người. Bởi vì nếu bạn nghĩ về nó một cách logic, họ không thực sự quan tâm đến việc chữa bệnh cho chúng ta!!! Nếu bệnh nhân nào cũng khỏe mạnh thì bác sĩ dược sĩ lấy đâu ra nguồn kiếm tiền nữa!&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE192" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tôi cũng đã từng sử dụng Diafinol. Tôi bị tiểu đường dù mới chỉ 49 tuổi, tôi không muốn chết sớm một chút nào!!!<br></h3>
                    </div>
                    <div id="IMAGE235" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE236" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Thanh Thủy&nbsp;</h3>
                    </div>
                    <div id="IMAGE238" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE239" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Minh Khánh&nbsp;</h3>
                    </div>
                    <div id="IMAGE240" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE241" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đình Khanh&nbsp;</h3>
                    </div>
                    <div id="HEADLINE283" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><b class="today"></b></h3>
                    </div>
                    <div id="HEADLINE284" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><b class="today"></b></h3>
                    </div>
                    <div id="HEADLINE285" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-1)</SCRIPT></SPAN></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION193" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE194" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tôi đã dùng Metformin trong 7 năm. Và không có kết quả. lượng đường trong máu vẫn cứ tăng liên tục. Nó chỉ ngày càng trở nên tồi tệ hơn. Và rồi tôi biết đến Diafinol. Nó giống như trời và đất. Do đó, đối với tất cả những ai đọc những dòng này, tôi khuyên bạn nên đặt hàng Diafinol khi nó vẫn còn có sẵn. Loại thuốc này sẽ cứu bạn!<br></h3>
                    </div>
                    <div id="HEADLINE195" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Cảm ơn, tôi đã đặt hàng. Rất ngạc nhiên với mức giá này. Tôi sẽ cố gắng điều trị, tôi đã quá mệt mỏi với bệnh tiểu đường, đây không phải cuộc sống mà là một cơn ác mộng!<br></h3>
                    </div>
                    <div id="HEADLINE196" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Sản phẩm tuyệt vời để giảm lượng đường. Trong một tháng, lượng đường của tôi giảm từ 14 mmol / l xuống còn 7,9! Mất 2 tuần!<br></h3>
                    </div>
                    <div id="HEADLINE197" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đã đặt hàng. Họ hứa là ngày kia tôi sẽ có thể nhận được. Tôi sẽ bắt đầu điều trị ngay.<br></h3>
                    </div>
                    <div id="IMAGE242" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE243" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Hoàng Long</h3>
                    </div>
                    <div id="HEADLINE244" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đồng Mỵ</h3>
                    </div>
                    <div id="IMAGE246" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE247" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Văn Minh&nbsp;</h3>
                    </div>
                    <div id="HEADLINE248" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Vinh Thành&nbsp;</h3>
                    </div>
                    <div id="IMAGE249" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="IMAGE250" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE286" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-1)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE287" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-2)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE288" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-2)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE289" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-2)</SCRIPT></SPAN></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION198" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE199" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Cảm ơn nhà bác sĩ về chia sẻ này. Tôi cũng không hiểu tại sao không ai sử dụng một khám phá tuyệt vời như vậy. Nhiều người chết vì bệnh tiểu đường, nhưng không ai quan tâm! Hóa ra vì các bác sĩ và dược sĩ chỉ quan tâm đến lợi nhuận cá nhân.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE200" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bạn nói đúng, sẽ không ai cảm thấy tiếc cho bệnh nhân tiểu đường, ngoại trừ chính họ. Chữa bệnh tiểu đường là điều nên làm. Tôi có nhiều người bạn mắc bệnh tiểu đường. Không ai trong số họ sống lâu hơn 70! Hầu hết chết sau 60, một người thì mất ở tuổi 68.<br></h3>
                    </div>
                    <div id="HEADLINE201" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Vâng, bệnh tiểu đường không phải là một món quà ... Mà là một hình phạt. Nó vắt kiệt tôi!<br></h3>
                    </div>
                    <div id="HEADLINE202" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bệnh tiểu đường có thể chữa khỏi bằng Diafinol. Nó đã giúp tôi rất nhiều, giờ tôi không còn bị tiểu đường nữa. Tôi đã giới thiệu Diafinol cho một vài người bạn của mình. Trong số 4 người, chỉ có một người không hồi phục! Nhưng tất cả đều giống nhau, lượng đường của họ đều bắt đầu giảm đi. Hãy thử Diafinol và nó có thể giúp bạn, đặc biệt là vì loại thuốc này khá rẻ.<br></h3>
                    </div>
                    <div id="IMAGE251" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE252" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đình Nghĩa&nbsp;</h3>
                    </div>
                    <div id="HEADLINE253" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tâm Trần&nbsp;</h3>
                    </div>
                    <div id="IMAGE254" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE255" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Hoàng Hải&nbsp;</h3>
                    </div>
                    <div id="IMAGE256" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE257" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Đức Đại&nbsp;</h3>
                    </div>
                    <div id="IMAGE258" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE290" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-3)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE291" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-3)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE292" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-3)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE293" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-4)</SCRIPT></SPAN></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION203" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE204" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Diafinol ĐÃ HẾT!!!!!!! Tôi không còn thời gian để đặt hàng. Họ nói nó đã hết từ hôm qua.... Bệnh tiểu đường đã làm tôi kiệt sức. Tôi không biết làm thế nào để điều trị. Thật may khi giờ đây đã có Diafinol<br></h3>
                    </div>
                    <div id="HEADLINE205" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Một người bạn đã giới thiệu Diafinol cho tôi, cô ấy đã khen sản phẩm này rất nhiều nhưng tôi không biết đặt hàng ở đâu, điều đó làm tôi cực kỳ khó chịu.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE206" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Chào mọi người. Tôi 59 tuổi, cân nặng ban đầu là 124 kg, cân nặng hiện nay là 80 kg, cao 168 cm, mức đường huyết dao động từ 18 đến 12. Bây giờ là 5,0. Tôi đã dùng Diafinol trong 3 tháng. Hơn cả hài lòng với kết quả này. Loại thuốc này đã giúp tôi rất nhiều.<br></h3>
                    </div>
                    <div id="HEADLINE208" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Mọi người chú ý! Diafinol ĐÃ ĐƯỢC BÁN TRỞ LẠI! Theo trang web, có khoảng 200 hộp Diafinol có sẵn. Nhưng chỉ còn vài hộp cuối cùng, vì số tiền phân bổ cho chương trình đã hết.<br></h3>
                    </div>
                    <div id="HEADLINE209" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Thuốc thực sự tuyệt vời. Tôi đã dùng nó được một tuần và tôi đã thấy những thay đổi tích cực. Tôi đo lượng đường nhiều lần trong ngày. Nó tăng ít hơn. Nếu nó diễn ra như thế này, thì sẽ có mọi cơ hội để phục hồi.<br></h3>
                    </div>
                    <div id="HEADLINE259" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Vũ Thúy&nbsp;</h3>
                    </div>
                    <div id="IMAGE260" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE261" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Kim Hiền&nbsp;</h3>
                    </div>
                    <div id="IMAGE262" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE263" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Mai Ly&nbsp;</h3>
                    </div>
                    <div id="IMAGE264" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE265" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Mỹ Thuận&nbsp;</h3>
                    </div>
                    <div id="IMAGE266" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE267" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bình Minh&nbsp;</h3>
                    </div>
                    <div id="IMAGE268" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE294" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-4)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE295" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-4)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE296" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-4)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE297" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-4)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE298" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-4)</SCRIPT></SPAN></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION210" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="HEADLINE211" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bệnh tiểu đường là một căn bệnh rất ngấm ngầm. Tôi bị 4 năm rồi, không có biểu hiện gì bên ngoài, chỉ thỉnh thoảng bị khô miệng thôi.  Nhưng gần đây tôi đã bị ngất xỉu. Họ đưa tôi đến bệnh viện để làm các xét nghiệm. Mọi rất tồi tệ, thận của tôi đã ở tình trạng tiền ung thư, các mạch bị bào mòn khiến các bác sĩ vô cùng sốc.&nbsp;<br></h3>
                    </div>
                    <div id="HEADLINE212" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Bố tôi bị tiểu đường và không muốn uống thuốc. Có ai biết nếu Diafinol có thể giúp bố tôi được hay không?<br></h3>
                    </div>
                    <div id="HEADLINE213" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tất nhiên nó sẽ hiệu quả rồi! Trong mọi trường hợp, tốt hơn là nên thử, đặc biệt là khi Diafinol rẻ hơn nhiều so với Metformin.<br></h3>
                    </div>
                    <div id="HEADLINE214" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Diafinol là một loại thuốc tuyệt vời. Con gái chúng tôi được chẩn đoán mắc bệnh tiểu đường. Con bé mới chỉ 7 tuổi. Chúng tôi không muốn sử dụng các loại thuốc hóa học khi con bé còn nhỏ. Tham khảo ý kiến ​​​​với nhiều bác sĩ nội tiết. Chúng tôi quyết định chọn Diafinol. Loại thuốc này đã giúp con gái tôi rất tốt. Đường gần như ngừng tăng. Hiện tại, con bé không còn cần uống Diafinol nữa.<br></h3>
                    </div>
                    <div id="HEADLINE269" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Văn Đạt&nbsp;</h3>
                    </div>
                    <div id="HEADLINE270" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Hoa Mai&nbsp;</h3>
                    </div>
                    <div id="HEADLINE271" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Mỹ Duyên&nbsp;</h3>
                    </div>
                    <div id="HEADLINE272" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Thạch Thảo&nbsp;</h3>
                    </div>
                    <div id="IMAGE276" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="IMAGE277" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="IMAGE278" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="IMAGE279" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE299" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-5)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE300" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-5)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE301" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-5)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE302" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-5)</SCRIPT></SPAN></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION215" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="PARAGRAPH216" class="ladi-element">
                        <div class="ladi-paragraph ladi-transition">Cảm ơn bạn! Tôi đã đặt mua 10 hộp bằng mọi giá.&nbsp;</div>
                    </div>
                    <div id="PARAGRAPH217" class="ladi-element">
                        <div class="ladi-paragraph ladi-transition">Bạn mua được 10 hộp nhưng sẽ có ai đó chẳng thể mua được hộp nào&nbsp;
                            <br>
                        </div>
                    </div>
                    <div id="PARAGRAPH218" class="ladi-element">
                        <div class="ladi-paragraph ladi-transition">Thanks. Tôi đã để lại thông tin đăng ký.
                            <br>
                        </div>
                    </div>
                    <div id="HEADLINE273" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Ngọc Nữ&nbsp;</h3>
                    </div>
                    <div id="HEADLINE274" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Kim Anh&nbsp;</h3>
                    </div>
                    <div id="HEADLINE275" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Dũng Bùi&nbsp;</h3>
                    </div>
                    <div id="IMAGE280" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="IMAGE281" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="IMAGE282" class="ladi-element">
                        <div class="ladi-image ladi-transition">
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE303" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-6)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE304" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-6)</SCRIPT></SPAN></h3>
                    </div>
                    <div id="HEADLINE305" class="ladi-element ldp-js-date2">
                        <h3 class="ladi-headline ladi-transition"><span><script>fdate(-6)</SCRIPT></SPAN></h3>
                    </div>
                    <div data-action="true" id="BUTTON330" class="ladi-element">
                        <div class="ladi-button ladi-transition">
                            <div class="ladi-button-background"></div>
                            <div id="BUTTON_TEXT330" class="ladi-element ladi-button-headline">
                                <p class="ladi-headline ladi-transition">Đặt hàng Diafinol với giá thấp nhất</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SECTION219" class="ladi-section">
                <div class="ladi-section-background"></div>
                <div class="ladi-container">
                    <div id="BOX223" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div id="BOX225" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div id="HEADLINE226" class="ladi-element">
                        <h3 class="ladi-headline ladi-transition">Tin CO trên mạng xã hội:</h3>
                    </div>
                    <div data-action="true" id="BOX227" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="BOX228" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="BOX229" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="BOX230" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="BOX231" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="BOX232" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="BOX233" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div data-action="true" id="BOX234" class="ladi-element">
                        <div class="ladi-box ladi-transition"></div>
                    </div>
                    <div id="HEADLINE330" class="ladi-element">
                        <h3 class="ladi-headline">Sản phẩm không phải là thuốc và không có tác dụng thay thế thuốc chữa bệnh<br></h3>
                    </div>
                </div>
            </div>
            <div id="SECTION331" class="ladi-section">
                <div class="ladi-section-background loaded"></div>
                <div class="ladi-container">
                    <div id="PARAGRAPH333" class="ladi-element">
                        <div class="ladi-paragraph ladi-transition">© Copyright © 2024</div>
                    </div>
                    <div id="GROUP331" class="ladi-element">
                        <div class="ladi-group"><a href="https://www.healthchoice.asia/dieukhoandieukien-fix" target="_blank" id="BUTTON331" class="ladi-element"><div class="ladi-button ladi-transition"><div class="ladi-button-background"></div><div id="BUTTON_TEXT331" class="ladi-element ladi-button-headline"><p class="ladi-headline ladi-transition">Điều khoản sử dụng&nbsp;</p></div></div></a>
                            <div id="LINE330" class="ladi-element">
                                <div class="ladi-line">
                                    <div class="ladi-line-container"></div>
                                </div>
                        </div><a href="https://www.healthchoice.asia/tuyenbotuchoitrachnhiem-vnfix" target="_blank" id="BUTTON332" class="ladi-element"><div class="ladi-button ladi-transition"><div class="ladi-button-background"></div><div id="BUTTON_TEXT332" class="ladi-element ladi-button-headline"><p class="ladi-headline ladi-transition">Tuyên bố từ chối trách nhiệm</p></div></div></a>
                        <div id="LINE331" class="ladi-element">
                            <div class="ladi-line">
                                <div class="ladi-line-container"></div>
                            </div>
                    </div><a href="https://www.healthchoice.asia/chinhsachquyenriengtu-fix" target="_blank" id="BUTTON333" class="ladi-element"><div class="ladi-button ladi-transition"><div class="ladi-button-background"></div><div id="BUTTON_TEXT333" class="ladi-element ladi-button-headline"><p class="ladi-headline ladi-transition">Chính sách bảo mật</p></div></div></a>
                </div>
            </div>
        </div>
        </div>
        <div id="SECTION_POPUP" class="ladi-section">
            <div class="ladi-section-background"></div>
            <div class="ladi-container">
                <div id="POPUP306" class="ladi-element spin-result-wrapper">
                    <div class="ladi-popup">
                        <div class="ladi-popup-background"></div>
                        <div class="ladi-overlay"></div>
                        <div id="SHAPE307" class="ladi-element">
                            <div class="ladi-shape ladi-transition">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 24 24" class="" fill="rgba(22, 199, 46, 1)">
                                    <path d="M10,17L5,12L6.41,10.58L10,14.17L17.59,6.58L19,8M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"></path>
                                </svg>
                            </div>
                        </div>
                        <div id="HEADLINE308" class="ladi-element">
                            <h3 class="ladi-headline ladi-transition">Chúc mừng!<br></h3>
                        </div>
                        <div id="HEADLINE309" class="ladi-element">
                            <h3 class="ladi-headline ladi-transition">Bạn đã nhận được giảm giá -50% cho sản phẩm Diafinol</h3>
                        </div>
                        <div data-action="true" id="BUTTON310" class="ladi-element pop-button-door" data-cy="clickToForm">
                            <div class="ladi-button ladi-transition">
                                <div class="ladi-button-background"></div>
                                <div id="BUTTON_TEXT310" class="ladi-element ladi-button-headline">
                                    <p class="ladi-headline ladi-transition">OK</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="POPUP330" class="ladi-element">
                    <div class="ladi-popup">
                        <div class="ladi-popup-background"></div>
                        <div id="PARAGRAPH332" class="ladi-element">
                            <div class="ladi-paragraph">Chúng tôi là một trang web đánh giá chuyên nghiệp nhận tiền hoa hồng từ
                                các công ty có sản phẩm mà chúng tôi đánh giá. Chúng tôi kiểm tra kỹ lưỡng
                                từng sản phẩm và chỉ chấm điểm cao cho những sản phẩm tốt nhất. Chúng tôi
                                thuộc sở hữu độc lập và các ý kiến được bày tỏ ở đây là thuộc sở hữu của
                                riêng chúng tôi.
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div id="backdrop-popup" class="backdrop-popup"></div>
        <div id="backdrop-dropbox" class="backdrop-dropbox"></div>
        <div id="lightbox-screen" class="lightbox-screen"></div>
        <script src="js/spin.js" type="text/javascript"></script>
        <script src="js/notify.js" type="text/javascript"></script>
        <script src="js/custom.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            $('#BUTTON_TEXT40').click(function(e){
                var str = $('#FORM39').find('input[name="phone"]').val();
                var trim = str.replace(/[_\W]+/g, '');
                intRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g;
                if(trim.length > 13 || trim.length < 8)
                {
                  alert("The phone number must more than 8 and less than 14");
                  e.preventDefault();
                  return false;
                }
                if(!intRegex.test(trim))
                {
                  alert('Please enter a valid phone number.');
                  e.preventDefault();
                  return false;
                }
              });
              $('#FORM39').find('input[name="phone"]').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                  var str = $('#FORM39').find('input[name="phone"]').val();
                  var trim = str.replace(/[_\W]+/g, '');
                  intRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g;
                  if(trim.length > 13 || trim.length < 8)
                  {
                    alert("The phone number must more than 8 and less than 14");
                    e.preventDefault();
                    return false;
                  }
                  if(!intRegex.test(trim))
                  {
                    alert('Please enter a valid phone number.');
                    e.preventDefault();
                    return false;
                  }
                }
              });
        </script>
        <script>
            $(function () {
                $("a").click(function (event) {
                    if (this.hash !== "") {
                    event.preventDefault();
                    const hash = this.hash;
                    $("html").animate(
                        {
                            scrollTop: $(hash).offset().top
                        },800 );
                    }
                });
            });
         
            var t = document.getElementsByTagName("a");
            for( var x of t){
                x.removeAttribute("data-replace-href");
            }
            $('.ladi-spin-lucky-start').click(function(){
                $('a').each(function(){
                    for( var x of t){
                        x.removeAttribute("href");
                    }
                });
            });
        </script>
      
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                var today = new Date();
                var date = today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear();
                var moreDay = document.querySelectorAll(".today");
                for( var moreD of moreDay ){
                moreD.innerHTML = date;
                }
            });
        </script>
        <div id="NOTIFY490" class="ladi-element ladi-notify-transition" style="top:10px;opacity: 0;">
            <div class="ladi-notify">
                <div class="ladi-notify-image">
                    <img alt="" src="js/spiner.gif">
                </div>
                <div class="ok">
                    <div class="ladi-notify-title">Nội dung cột [Title]</div>
                    <div class="ladi-notify-content">Nội dung cột [Content]</div>
                    <div class="ladi-notify-time">Nội dung cột [Time]</div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            window.ladi_viewport = function () {
                var screen_width = window.ladi_screen_width || window.screen.width;
                var width = window.outerWidth > 0 ? window.outerWidth : screen_width;
                var widthDevice = width;
                var is_desktop = width >= 768;
                var content = "";
                if (typeof window.ladi_is_desktop == "undefined" || window.ladi_is_desktop == undefined) {
                    window.ladi_is_desktop = is_desktop;
                }
                if (!is_desktop) {
                    widthDevice = 420;
                } else {
                    widthDevice = 1200;
                }
                content = "width=" + widthDevice;
                var scale = 1;
                if (!is_desktop && widthDevice != screen_width && screen_width > 0) {
                    scale = screen_width / widthDevice;
                }
                if (scale != 1) {
                    content += ", initial-scale=1.0, minimum-scale=0.5, maximum-scale=3.0";
                }
                var docViewport = document.getElementById("viewport");
                if (!docViewport) {
                    docViewport = document.createElement("meta");
                    docViewport.setAttribute("id", "viewport");
                    docViewport.setAttribute("name", "viewport");
                    document.head.appendChild(docViewport);
                }
                docViewport.setAttribute("content", content);
            };
            window.ladi_viewport();
            window.ladi_fbq_data = [];
            window.ladi_fbq = function (track_name, conversion_name, data, event_data) {
                window.ladi_fbq_data.push([track_name, conversion_name, data, event_data]);
            };
        </script>
        <script>
            $( ".sendform" ).submit(function( event ) {
                event.preventDefault();
                const name  = $('form .name').val();
                const phone  = $('form .phone').val();
                const name1 = $('form .name1').val();
                const phone1  = $('form .phone1').val();
                localStorage.setItem('nameldp', name ? name : name1);
                localStorage.setItem('phoneldp', phone ? phone : phone1);
                setTimeout(() => {
                    window.location = "js/thank.html";
                }, 1);
            });
        </script>
        <script>
            document.querySelectorAll('div[data-action="true"], a:not([target="_blank"]').forEach(function (link) {
                  link.addEventListener('click', function (e) {
                      e.preventDefault();
                      document.querySelector('#SECTION321').scrollIntoView({behavior: "smooth", block: "center"});
                  });
              });
        </script>
        <script>
        $('#PARAGRAPH331').click(function() {
           $('#POPUP330').attr('style', 'display: block !important');
           $('#backdrop-popup').attr('style', 'display: block !important; background-color : rgba(0, 0, 0, 0.5)');
       })
        $('#backdrop-popup').click(function() {
           $('#POPUP330').attr('style', 'display: none !important');
           $('#backdrop-popup').attr('style', 'display: none !important; background-color : rgba(0, 0, 0, 0.5)');
       })
   </script>
    



</body></html><!--Publish time: Fri, 19 Apr 2024 11:01:35 GMT--><!--LadiPage build time: Fri, 19 Apr 2024 08:11:11 GMT-->