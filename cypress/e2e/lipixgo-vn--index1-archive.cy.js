describe('Archive Landing Page', () => {
    beforeEach(() => {
      // Thay 'yourpage.html' bằng đường dẫn tới trang của bạn
      cy.visit('./public/lipixgo-vn--index1/index.html') 
    })
  
    it('Contains specific content', () => {
  
      cy.wait(1000)
      
      // Check data
      cy.get('[data-cy="data1"]').contains('Ổn định huyết áp trong 6 giờ đầu tiên').should('exist')
      cy.wait(1000)
  
      cy.get('[data-cy="price"]').contains('Giá khuyến mại: 590.000 VNĐ').should('exist')
      cy.wait(1000)

      // click CTA
      cy.get('[data-cy="clickCTA"]').click()
      cy.wait(2000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION146').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION1').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
    })
  
    it('Test form', () => {
  
      cy.wait(1000)
      cy.get('[data-cy="name"]').type('test')
  
      cy.wait(1000)
      cy.get('[data-cy="phone"]').type('0123547593djfhdf')
  
      cy.wait(1000)
      cy.get('[data-cy="submit"]').click()
  
      cy.wait(2000)
      cy.get('[data-cy="phone"]').clear().type('0978456828')
  
      cy.wait(1000)
      cy.get('[data-cy="submit"]').click()
  
      cy.wait(3000)
      cy.go('back')
  
    })
  
    it('Test on mobile', () => {
  
      cy.viewport(430, 932)
      cy.reload()
      cy.wait(3000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION146').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION1').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
      // test form
      cy.wait(1000)
      cy.get('[data-cy="name"]').type('test')
  
      cy.wait(1000)
      cy.get('[data-cy="phone"]').type('0123547593djfhdf')
  
      cy.wait(1000)
      cy.get('[data-cy="submit"]').click()
  
      cy.wait(2000)
      cy.get('[data-cy="phone"]').clear().type('0978456828')
  
      cy.wait(1000)
      cy.get('[data-cy="submit"]').click()
  
      cy.wait(3000)
      cy.go('back')
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION1').offset().top
        }, 5000)
      })
  
    })
  
  })
  