describe('Archive Landing Page', () => {
    beforeEach(() => {
      // Thay 'yourpage.html' bằng đường dẫn tới trang của bạn
      cy.visit('http://localhost/LDP/cypress/landing-e2e/public/diafinol-vn-dr-19424/index.php') 
    })
  
    it('Contains specific content', () => {
  
      cy.wait(1000)
      
      // Check data
      cy.get('[data-cy="data1"]').contains('Glucose ở nồng độ cao ảnh hưởng xấu tới các tế bào sống').should('exist')
      cy.wait(2000)

      // click CTA
      cy.get('[data-cy="clickCTA"]').click()
      cy.wait(2000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION185').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION8').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
    })
  
    it('Test form', () => {
        cy.wait(200)
        cy.get('.ladi-spin-lucky-start').click()
        cy.wait(8000)
        
        cy.get('[data-cy="clickToForm"]').click()
  
        cy.wait(1000)
        cy.get('[data-cy="name"]').type('Hùng Anh')
    
        cy.wait(1000)
        cy.get('[data-cy="phone"]').type('0123547593djfhdf')
    
        cy.wait(1000)
        cy.get('[data-cy="submit"]').click()
    
        cy.wait(2000)
        cy.get('[data-cy="phone"]').clear().type('0978454637')
    
        cy.wait(1000)
        cy.get('[data-cy="submit"]').click()
    
        cy.wait(3000)
        cy.go('back')
  
    })
  
    it('Test on mobile', () => {
  
      cy.viewport(430, 932)
      cy.reload()
      cy.wait(3000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION185').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
      cy.window().then((win) => {
        win.jQuery('html, body').animate({
          scrollTop: win.jQuery('#SECTION8').offset().top
        }, 5000)
      })
      cy.wait(5000)
  
      // test form
        cy.get('.ladi-spin-lucky-start').click()
        cy.wait(8000)
        cy.get('[data-cy="clickToForm"]').click()
  
        cy.wait(1000)
        cy.get('[data-cy="name"]').type('Minh Anh')
    
        cy.wait(1000)
        cy.get('[data-cy="phone"]').type('0123547593djfhdf')
    
        cy.wait(1000)
        cy.get('[data-cy="submit"]').click()
    
        cy.wait(2000)
        cy.get('[data-cy="phone"]').clear().type('0978458845')
    
        cy.wait(1000)
        cy.get('[data-cy="submit"]').click()
    
        cy.wait(3000)
        cy.go('back')

        cy.window().then((win) => {
            win.jQuery('html, body').animate({
            scrollTop: win.jQuery('#SECTION8').offset().top
            }, 5000)
        })
  
    })
  
  })
  